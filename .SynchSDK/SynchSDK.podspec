Pod::Spec.new do |s|
  s.name             = 'SynchSDK'
  s.version          = '1.2.8'
  s.summary          = 'A short description of WatchTogetherLib.'

  s.homepage         = 'https://www.sceenic.co/'
  # s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'George' => 'support@sceenic.co' }
  s.source           = { :url => 'https://media.sceenic.co/', :tag => s.version.to_s }

  s.ios.deployment_target = '11.0'

  s.dependency 'Starscream'
  
  s.source_files = "SynchSDK.framework/Headers/*.h"
  s.vendored_frameworks = "SynchSDK.framework"

  s.static_framework = true
end

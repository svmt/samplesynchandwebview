//
//  LoginViewController.swift
//  ExampleSynchSDK
//
//  Created by Anton Umnitsyn on 04.08.2021.
//

import UIKit


enum UserDefaultsKey {
    static let userName = "userName"
    static let translationName = "translationName"
    static let apiKey = "apiKey"
    static let apiSecret = "apiSecret"
    static let stream = "stream"
}

class LoginViewController: UIViewController {
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var apiKeyTextField: UITextField!
    @IBOutlet weak var apiSecretTextField: UITextField!
    @IBOutlet weak var casTextField: UITextField!
    @IBOutlet weak var accessTokenTextField: UITextField!
    @IBOutlet weak var streamTextField: UITextField!
    private var accessToken: String?
    
    let streamLinks = [["Footbal":"https://demo-app.sceenic.co/football.m3u8"],
                       ["CDN":"https://devstreaming-cdn.apple.com/videos/streaming/examples/bipbop_4x3/bipbop_4x3_variant.m3u8"]]
    private var streamPicker: UIPickerView!

    override func viewDidLoad() {
        super.viewDidLoad()
        setFieldsUI()
        let tapRecogniser = UITapGestureRecognizer()
        tapRecogniser.addTarget(self, action: #selector(closeKeyboard))
        view.addGestureRecognizer(tapRecogniser)
        streamPicker = UIPickerView()
        streamPicker.dataSource = self
        streamPicker.delegate = self
        streamTextField.inputView = streamPicker
        streamTextField.text = streamLinks[0].values.first
        setTextFields()
    }

    private func setFieldsUI() {
        let fieldsArray = [
            userNameTextField,
            apiKeyTextField,
            apiSecretTextField,
            casTextField,
            streamTextField
        ]
        for field in fieldsArray {
            field?.layer.borderColor = UIColor.systemBlue.cgColor
            field?.layer.borderWidth = 0.5
            field?.layer.cornerRadius = 5.0
        }
    }

    override func viewWillAppear(_ animated: Bool) {

    }

    /// Set textfields text to stored data form UserDefaults
    private func setTextFields() {
        guard let groupID = UserDefaults.standard.string(forKey: UserDefaultsKey.userName),
              let translationName = UserDefaults.standard.string(forKey: UserDefaultsKey.translationName),
              let token = UserDefaults.standard.string(forKey: UserDefaultsKey.apiKey),
              let sToken = UserDefaults.standard.string(forKey: UserDefaultsKey.apiSecret),
              let stream = UserDefaults.standard.string(forKey:   UserDefaultsKey.stream)
        else { return }
        userNameTextField.text = groupID
        casTextField.text = translationName
        apiKeyTextField.text = token
        apiSecretTextField.text = sToken
        streamTextField.text = stream
    }


    /// Store text fields data in UserDefaults
    private func storeCredentials() {
        UserDefaults.standard.setValue(userNameTextField.text, forKey: UserDefaultsKey.userName)
        UserDefaults.standard.setValue(casTextField.text, forKey: UserDefaultsKey.translationName)
        UserDefaults.standard.setValue(apiKeyTextField.text, forKey: UserDefaultsKey.apiKey)
        UserDefaults.standard.setValue(apiSecretTextField.text, forKey: UserDefaultsKey.apiSecret)
        UserDefaults.standard.setValue(streamTextField.text, forKey: UserDefaultsKey.stream)
    }


    @objc private func closeKeyboard() {
        view.endEditing(true)
    }

    @IBAction func loginAction(_ sender: Any) {
        if let text = accessTokenTextField.text, !text.isEmpty, !(userNameTextField.text?.isEmpty ?? true) {
            if let (token, stream) = extractTokenAndStream(from: text) {
                self.showSession(with: token, stream: stream)
                return
            }
        }
        
        if apiKeyTextField.text!.isEmpty || casTextField.text!.isEmpty || userNameTextField.text!.isEmpty {
            let alert = UIAlertController(title: "Mandatory fields", message: "Fields can't be empty!", preferredStyle: .alert)
            let action = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
            alert.addAction(action)
            self.present(alert, animated: true, completion: nil)
            return
        }
        let tokenManager = TokenManager()
        
        tokenManager.getGroutedSocketUrl(to: casTextField.text!, with: ["Auth-API-Key" : apiKeyTextField.text!, "Auth-API-Secret" : apiSecretTextField.text!]) { [weak self] response in
            DispatchQueue.main.async {
                if let token = response {
                    let stream = self?.streamTextField.text!
                    self?.showSession(with: token, stream: stream ?? self!.streamLinks[0].values.first!)
                }
            }
        }
        
    }
    
    func extractTokenAndStream(from text: String) -> (token: String, stream: String)? {
        guard  let url = URL(string: text),
               let token = url.queryParameters!["token"],
               let stream = url.queryParameters!["stream"]
            else { return nil}
        
        return (token: token, stream: stream)
//        return text.components(separatedBy: "/").last
    }
    
    private func showSession(with token: String, stream: String) {
        var vc: ViewController
        if #available(iOS 13.0, *) {
            vc = self.storyboard?.instantiateViewController(identifier: "mainView") as! ViewController
        } else {
            vc = ViewController()
        }

        vc.accessToken = token
        vc.streamURLString = streamTextField.text
        vc.userName = userNameTextField.text
        
        
        storeCredentials()
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension LoginViewController: UIPickerViewDataSource, UIPickerViewDelegate {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        2
    }

    func pickerView( _ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return streamLinks[row].keys.first
//        switch row {
//        case 0:
//            return "https://devstreaming-cdn.apple.com/videos/streaming/examples/bipbop_4x3/bipbop_4x3_variant.m3u8"
//        default:
//            return "https://demo-app.sceenic.co/football.m3u8"
//        }
    }

    func pickerView( _ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        casTextField.text = streamLinks[row].values.first
    }
}


extension URL {
    public var queryParameters: [String: String]? {
        guard
            let components = URLComponents(url: self, resolvingAgainstBaseURL: true),
            let queryItems = components.queryItems else { return nil }
        return queryItems.reduce(into: [String: String]()) { (result, item) in
            result[item.name] = item.value
        }
    }
}

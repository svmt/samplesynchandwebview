//
//  ViewController.swift
//  synchSDKPlayers
//
//  Created by User on 7/7/21.
//

import UIKit
import AVKit
import SynchSDK
import CallKit
import WebKit

class ViewController: UIViewController, CXCallObserverDelegate {
    @IBOutlet weak var playControlStack: UIStackView!
    @IBOutlet weak var seekControlStack: UIStackView!
    @IBOutlet weak var roomInfoStack: UIStackView!
    @IBOutlet weak var clientsLabel: UILabel!
    @IBOutlet weak var accuracyLabel: UILabel!
    @IBOutlet weak var deltaLabel: UILabel!
    @IBOutlet weak var videoContainer: UIView!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var pauseButton: UIButton!
    @IBOutlet weak var syncOnButton: UIButton!
    @IBOutlet weak var syncOffButton: UIButton!
//    @IBOutlet weak var wkWebView: WKWebView!
    var wkWebView: WKWebView!
    var player: AVPlayer?
    var playerLayer: AVPlayerLayer?
    var webViewActive: Bool = false

    var accessToken: String!
    var streamURLString: String!
    var userName: String!
    var syncSDK: SynchSDK?

    private var  tokenString: String!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        configPlayer()
        setUpSynch()
        pauseButton.isEnabled = false
        syncOffButton.isHidden = true
        setupObservers()
    }

    override func viewDidAppear(_ animated: Bool) {
        super .viewDidAppear(animated)
        showWebView(self)
    }

    private func addWebView() {
        let contentController = WKUserContentController();
        contentController.add(self, name: "callbackHandler")
        let config = WKWebViewConfiguration()
        config.userContentController = contentController
        config.allowsInlineMediaPlayback = true
        config.allowsPictureInPictureMediaPlayback = false
//        player?.isMuted = true
        let audioSession = AVAudioSession.sharedInstance()
        try! audioSession.setCategory(.playback, mode: .moviePlayback, options: [.mixWithOthers])
//        try! audioSession.overrideOutputAudioPort(AVAudioSession.PortOverride.speaker)
//        try! audioSession.setCategory(., mode: .videoRecording, options: [.mixWithOthers])
        try! audioSession.setActive(true)
        wkWebView = WKWebView(frame: CGRect.zero, configuration: config)
        wkWebView.isHidden = true
        wkWebView.alpha = 0.5
        wkWebView.navigationDelegate = self
        wkWebView.uiDelegate = self
//        wkWebView.customUserAgent = "Mozilla/5.0 (iPhone; CPU iPhone OS 12_3_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.1.1 Mobile/15E148 Safari/604.1"
//        self.view.insertSubview(wkWebView, at: 1) //addSubview(wkWebView)
        self.view.addSubview(wkWebView)
        wkWebView.translatesAutoresizingMaskIntoConstraints = false
        let topConstraint = wkWebView.topAnchor.constraint(equalTo: videoContainer.topAnchor)
        let widthConstraint = wkWebView.widthAnchor.constraint(equalTo: videoContainer.widthAnchor)
        let horizontalCenterConstraint = wkWebView.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        let heightConstraint = wkWebView.heightAnchor.constraint(equalToConstant: 300)
        view.addConstraints([topConstraint, widthConstraint, horizontalCenterConstraint, heightConstraint])
    }

    private func copyToken() {
        UIPasteboard.general.string = accessToken
    }
    
    private func setupObservers() {
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(appMovedToBackground), name: UIApplication.willResignActiveNotification, object: nil)
        notificationCenter.addObserver(self, selector: #selector(appDidBecomeActive), name: UIApplication.didBecomeActiveNotification, object: nil)
    }

    deinit {
        let notificationCenter = NotificationCenter.default
        notificationCenter.removeObserver(self, name: UIApplication.willResignActiveNotification, object: nil)
        notificationCenter.removeObserver(self, name: UIApplication.didBecomeActiveNotification, object: nil)   
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }

    @IBAction func showWebView(_ sender: Any) {
        if webViewActive {
            wkWebView.removeFromSuperview()
            webViewActive = false
            return
        }
        addWebView()
        wkWebView.isHidden = false
        wkWebView.loadHTMLString("https://pure-js-websdk-demo.web.app/", baseURL: nil)
        guard let url = URL(string: "https://pure-js-websdk-demo.web.app/") else { return }
            
        let request = URLRequest(url: url)
        wkWebView.load(request)
    }

    func setUpSynch() {
        syncSDK = try? SynchSDK(accessToken: accessToken, userName: userName)
        syncSDK?.attachListener(self)
    }

    
    func startSync() {
        if syncSDK != nil {
            syncSDK?.startSynchronize()
        }
    }
    
    func stopSync() {
        syncSDK?.stopSynchronize()
    }
    
    func configPlayer() {
        guard let streamUrl = URL(string: streamURLString ) else {
            return
        }

        let asset = AVAsset(url: streamUrl)
        let playerItem = AVPlayerItem(asset: asset)
        player = AVPlayer(playerItem: playerItem)
        playerLayer = AVPlayerLayer(player: player)
        playerLayer?.frame = videoContainer.bounds //bounds of the view in which AVPlayer should be displayed
        playerLayer?.videoGravity = .resizeAspect
        videoContainer.layer.addSublayer(playerLayer!)
        player?.prepareForInterfaceBuilder()
    }

    @IBAction func goBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true);
    }

    @IBAction func playAction(_ sender: UIButton) {
        player?.play()
        syncSDK?.playerPlay()
        sender.isEnabled = false
        pauseButton.isEnabled = true
    }

    @IBAction func pauseAction(_ sender: UIButton) {
        player?.pause()
        syncSDK?.playerPause()
        playButton.isEnabled = true
        sender.isEnabled = false
    }
    
    @IBAction func startSynch(_ sender: UIButton) {
        startSync()
        sender.isHidden = true
        syncOffButton.isHidden = false
    }

    @IBAction func stopSynch(_ sender: UIButton) {
        stopSync()
        sender.isHidden = true
        syncOnButton.isHidden = false
    }

    @IBAction func seekBack(_ sender: UIButton) {
        let time = player?.currentTime()
        let newTime = CMTime(seconds: time!.seconds - 30, preferredTimescale: 1)
        player?.seek(to: newTime)
        syncSDK?.playerSeek(position: Int(newTime.seconds * 1000))
    }
    @IBAction func seekForward(_ sender: UIButton) {
        let time = player?.currentTime()
        let newTime = CMTime(seconds: time!.seconds - 30, preferredTimescale: 1)
        player?.seek(to: newTime)
        syncSDK?.playerSeek(position: Int(newTime.seconds * 1000))
    }

    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
       super.viewWillTransition(to: size, with: coordinator)
        coordinator.animate(alongsideTransition: { (context) in
        }) { [weak self] (context) in
            self?.videoContainer.frame.size = size
            self?.playerLayer?.frame.size = size
        }
    }

    @IBAction func shareTapped(_ sender: Any) {
        guard let token = accessToken, let streamURL = streamURLString else { return }
        let linkString = "https://sceenic-sync-test.app/?token=\(token)&stream=\(streamURL)"
        UIPasteboard.general.string = linkString
        if let link = NSURL(string: linkString) {
            let activityVC = UIActivityViewController(activityItems: [link], applicationActivities: nil)
        activityVC.excludedActivityTypes = [.airDrop, .addToReadingList, .assignToContact, .openInIBooks, .saveToCameraRoll]
            self.present(activityVC, animated: true, completion: nil)
        }
    }

    // MARK: Restore session after call or after background
    func callObserver(_ callObserver: CXCallObserver, callChanged call: CXCall) {
        if call.hasEnded {
            callEnded()
        } else {
            callStarted()
        }
    }

    @objc func appMovedToBackground() {
        disconnectFlow()
    }

    @objc func appDidBecomeActive() {
        reconnectFlow()
    }

    func callStarted() {
        disconnectFlow()
    }

    func callEnded() {
        reconnectFlow()
    }

    func disconnectFlow(){
        syncSDK?.stopSynchronize()
        disconnectWebView()
    }

    func reconnectFlow() {
        syncSDK?.startSynchronize()
        connectWebView()
    }
}

extension ViewController: SynchListener {
    func onClientList(clientList: [Client]) {
        clientsLabel.text = ""
        var i: Int = 0
        for client in clientList {
            clientsLabel.text! += "\(client.name)" + (clientList.count > i+1 ? "\n" : "")
            i += 1
        }
        print(clientList)
    }
    
    func onSetPlaybackRate(rate: Float) {
        player?.rate = rate
    }
    
    func onPlaybackFromPosition(position: Int, participantId: String) {
        player?.seek(to: CMTime(seconds: Double(position)/1000, preferredTimescale: 1))
    }
    
    func onPause(participantId: String) {
        playButton.isEnabled = true
        pauseButton.isEnabled = false
        player?.pause()
    }
    
    func onResumePlay(participantId: String) {
        playButton.isEnabled = false
        pauseButton.isEnabled = true
        player?.play()
    }
    
    func onGetPlayerPosition() -> Int {
        if let currentDate = player?.currentItem?.currentDate() {
            let timeInterval = currentDate.timeIntervalSince1970
            return Int(timeInterval * 1000)
        } else {//for VOD: currentTime()
            let currentTime = player?.currentTime()
            let timeInterval = currentTime?.seconds
            return Int((timeInterval ?? 0) * 1000)
        }
    }
    
    func onGetPlaybackRate() -> Float {
        return player!.rate
    }
    
    func onSyncInfo(accuracy: Float, delta: Int) {
        print("accuracy: ", accuracy, "delta: ", delta)
        accuracyLabel.text = "Accuracy: \(accuracy)"
        deltaLabel.text = "Delta: \(delta)"
    }
}

extension ViewController: WKNavigationDelegate, WKUIDelegate, WKScriptMessageHandler {
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        if message.name == "callbackHandler" {
            print(message.body)
        }
    }

    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        connectWebView()
    }
    
    func disconnectWebView() {
        wkWebView.evaluateJavaScript("disconnect()")
    }
    
    func connectWebView() {
        webViewActive = true
        self.tokenString = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJyaWQiOiJiMzBmZGJjYS03OGM4LTRkN2QtODlkOC05YWNmM2EyMjgzNjMiLCJ1bmN0IjoiMDA0ODlmNDQtOGU3OS00NTc5LThmZDktMGEwM2ZlNDg3YmNlIiwic2lncmVzcyI6Im1lc2gzLnMzLnNjZWVuaWMuY28iLCJleHAiOjE2MzI4NDg5MzZ9.vyPFxYhuA_ESj-Z54CpwsrN5b4Xji4r-ynrjDhd62H4"
        if let token = self.tokenString {
            wkWebView.evaluateJavaScript("document.getElementById('display_name').value = 'Test'") { result, error in
                print(result ?? "Oops")
            }
            wkWebView.evaluateJavaScript("document.getElementById('session_token').value = '\(token)'") { result, error in
                print(result ?? "Oops")
            }
            wkWebView.evaluateJavaScript("connect()")
        }
        
    }
}

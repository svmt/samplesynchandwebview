//
//  TokenManager.swift
//  ExampleSynchSDK
//
//  Created by admin on 15.09.2021.
//

import Foundation

class TokenManager {
    
//    let synchCas = "https://cas.s1.sceenic.co/sync/token/"
//    let wtCas = "https://cas.s1.sceenic.co/stream/token/v2/"
    
    func getGroutedSocketUrl(to url: String, with params: [String: String], completion: @escaping(String?) -> ()) {
        guard let baseURL = URL(string: url) else { return }
        var request = URLRequest(url: baseURL)
        request.httpMethod = "GET"
        for (key, value) in params {
            request.addValue(value, forHTTPHeaderField: key)
        }
//        request.addValue("Bearer \(token.tokenString)", forHTTPHeaderField: "Authorization")
//        request.addValue("application/json", forHTTPHeaderField: "content-type")
        URLSession.shared.dataTask(with: request) { data, _, error in
            if error != nil {
                completion(nil)
            } else {
                guard let json = try? JSONSerialization.jsonObject(with: data!, options: []),
                    let dict = json as? [String: String],
                    let token = dict["token"]
                else { return }
                completion(token)
            }
        }.resume()
    }
}
